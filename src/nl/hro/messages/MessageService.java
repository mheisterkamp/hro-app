package nl.hro.messages;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.preference.PreferenceManager;
import android.text.format.DateUtils;
import android.text.format.Time;
import android.util.Log;
import android.widget.Toast;

public class MessageService extends IntentService {

	private DatabaseHandler db;

	// User settings
	boolean warnMe; // Notificaties sturen
	boolean keepChecking; // Blijven peilen
	int userScheduleTime; // Volgende peiling doen

	private SimpleDateFormat df;
	Date now;
	String currentTime;

	// Globale waarden voor notifications
	private int notificationLocation;
	private String notificationMessage;
	private boolean enteredLocation = false;

	// Locaties
	private Location currentLocation;
	private Location closestLocation;
	// Afstand
	private double distance;

	// Boolean voor kapotte GPS
	private final boolean GPS_BROKEN = true;
	// Lat en long uit Dordrecht
	private final double homeLat = 51.80907;
	private final double homeLong = 4.66825;

	final static String urlLocations = "http://www.martinheisterkamp.nl/herkansing/locations.php";
	final static String urlMessages = "http://www.martinheisterkamp.nl/herkansing/messages.php";

	/*
	 * Constructor zonder parameters
	 */
	public MessageService() {
		super("MessageService");
	}

	/*
	 * Settings ophalen en aan de hand van user prefs de juiste methoden
	 * aanroepen
	 */
	@Override
	protected void onHandleIntent(Intent intent) {

		// Database handler ophalen
		db = new DatabaseHandler(this, null, null, 0);

		// Maak een simple date format met Nederlandse locale
		df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", new Locale("nl", "NL"));

		// Datum in Date en huidige tijd in string maken
		now = new Date();
		currentTime = df.format(now);

		// User settings ophalen en in het class geheugen zetten
		getUserSettings();

		// Als de user heeft aangegeven dat er gepeild mag blijven worden
		if (keepChecking) {

			// Locaties ophalen
			currentLocation = getCurrentLocation(GPS_BROKEN);
			closestLocation = getClosestLocation();

			// Afstand tussen huidige locatie en dichtstbijzijnde locatie
			distance = currentLocation.distanceTo(closestLocation);
			// Als afstand <= 100 meter
			if (distance <= 100) {

				// Naam van de locatie ophalen
				String[] splitLocation = closestLocation.toString().split(":");
				// Van de gesplitte locatienaam komt het ID;
				int targetLocationID = Integer.parseInt(splitLocation[1]);

				// Visited location maken
				createVisitedLocation(targetLocationID, 1);

				// aangeven dat de locatie bezocht is voor de volgende keer dat
				// de locatie gechecked wordt
				enteredLocation = true;

				// Een message in de database zetten en deze ook sturen als
				// notification
				createMessageAndNotification(targetLocationID);

			} else {

				// Als er een locatie bezocht is
				if (enteredLocation == true) {
					// De boolean weer op false zetten voor de volgende keer dat
					// de locatie binnen 100m is
					enteredLocation = false;

					// Als binnen x tijd, de tijd uit de laatst toegevoegde
					// locatie eerder ligt
					// en bij die laatst toegevoegde locatie was entered true
					// dan zijn we van die locatie weggegaan

					// Wordt maar 1x uitgevoerd want bij een nieuw record is
					// de locatie entered false
					leaveLocation();
					
				}
			}

			scheduleNextUpdate(userScheduleTime);
		}
	}
	
	/*
	 * leave location is de functie die wordt uitgevoerd als een locatie
	 * weer wordt gecheckt maar de locatie is niet gelijk aan wanneer
	 * je op een andere locatie bent binnen gekomen
	 */
	private void leaveLocation() {
		Cursor lastVisited = db.getLastVisited();
		lastVisited.moveToFirst();
		do {
			// Datum maken om te vergelijken
			try {
				String lastTime = lastVisited.getString(3);
				Date lastTimeAsDate;
				lastTimeAsDate = df.parse(lastTime);

				int ifEntered = lastVisited.getInt(2);

				// Als de laatste datum eerder ligt dan nu
				if (lastTimeAsDate.before(now) && ifEntered == 1) {

					// Visited location maken, dit keer gaan we weg
					// van de locatie
					createVisitedLocation(lastVisited.getInt(1), 0);

				}

			} catch (ParseException e) {
				e.printStackTrace();
			}

		} while (lastVisited.moveToNext());
	}

	/*
	 * Wanneer er een nieuw bericht is wordt het bericht in de database gezet
	 * en een notificatie gestuurd
	 */
	private void createMessageAndNotification(int targetLocationID) {
		try {
			String messagesURL = urlMessages + "?id=" + targetLocationID
					+ "&debug=true";
			JSONObject JSONMessages = (JSONObject) getData(messagesURL);
			// Laatste bericht ophalen van API en in de db zetten
			insertLatestMessage(JSONMessages, targetLocationID);
			// Notificatie met het bericht sturen
			sendNotification(warnMe);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	/*
	 * Een nieuwe entry maken van de visited location
	 */
	private void createVisitedLocation(int targetLocationID, int entered) {
		// Visited location maken en in DB zetten
		ContentValues visitedLocation = new ContentValues();
		visitedLocation.put("loc_id", targetLocationID);
		visitedLocation.put("entered", entered);
		visitedLocation.put("timestamp", currentTime);
		db.insert("visited_locations", visitedLocation);
	}

	/*
	 * User settings in het class geheugen zetten
	 */
	private void getUserSettings() {
		// User settings ophalen
		SharedPreferences sharedPrefs = PreferenceManager
				.getDefaultSharedPreferences(this);

		// User settings in vars zetten
		warnMe = sharedPrefs.getBoolean("warn-me", true);
		keepChecking = sharedPrefs.getBoolean("keep-checking", true);
		userScheduleTime = Integer.parseInt(sharedPrefs.getString(
				"check-every", ""));
	}

	/*
	 * Huidge locatie ophalen
	 */
	private Location getCurrentLocation(Boolean bool) {
		Location location;

		if (bool == true) {
			location = new Location("Home");
			location.setLatitude(homeLat);
			location.setLongitude(homeLong);
		} else {
			LocationManager lm = (LocationManager) this
					.getSystemService(LOCATION_SERVICE);
			Criteria criteria = new Criteria();
			criteria.setAccuracy(Criteria.ACCURACY_FINE);
			String provider = lm.getBestProvider(criteria, true);
			location = lm.getLastKnownLocation(provider);
		}

		return location;
	}

	/*
	 * Nieuwste bericht in de database zetten
	 */
	private void insertLatestMessage(JSONObject object, int locationId) {

		try {
			Iterator<?> keys = object.keys();
			while (keys.hasNext()) {
				String key = (String) keys.next();
				if (object.get(key) instanceof JSONObject) {

					// @todo als curtime eerder is dan timestamp

					Date now = new Date();
					String timestamp = object.getString("timestamp");
					Date messageTime = df.parse(timestamp);

					if (now.before(messageTime)) {

						// Notification message en locatie setten
						notificationLocation = locationId;
						notificationMessage = "HRO";

						// Nieuwe message ContentValues maken
						ContentValues message = new ContentValues();
						message.put("loc_id", locationId);
						message.put("message", object.getString("message"));
						message.put("timestamp", object.getString("timestamp"));

						// Plaats message in db
						db.insert("messages", message);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Notificatie naar de gebruiker sturen
	 */
	private void sendNotification(Boolean userPermission) {
		// Als de user heeft aangegeven dat berichten gestuurd mogen worden
		if (userPermission) {

			// Notification maken die je naar de app stuurt
			Intent notificationIntent = new Intent(this, MainActivity.class);
			PendingIntent notificationPI = PendingIntent.getActivity(this, 0,
					notificationIntent, 0);

			Notification notification = new Notification.Builder(this)
					.setContentTitle(
							"Nieuw bericht van "
									+ db.getLocationName(notificationLocation))
					.setContentText(notificationMessage)
					.setSmallIcon(R.drawable.ic_launcher)
					.setContentIntent(notificationPI).build();
			// Notification automatisch hiden na select
			notification.flags |= Notification.FLAG_AUTO_CANCEL;

			// Notification manager ophalen
			NotificationManager notificationManager = (NotificationManager) this
					.getSystemService(NOTIFICATION_SERVICE);
			notificationManager.notify(0, notification);

		}
	}

	/*
	 * Data van de API ophalen
	 */
	private JSONObject getData(String url) throws JSONException {

		StringBuilder builder = new StringBuilder();
		HttpClient client = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet(url);

		try {
			HttpResponse response = client.execute(httpGet);
			StatusLine statusLine = response.getStatusLine();
			int statusCode = statusLine.getStatusCode();
			if (statusCode == 200) {
				HttpEntity entity = response.getEntity();
				InputStream content = entity.getContent();
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(content));
				String line;
				while ((line = reader.readLine()) != null) {
					builder.append(line);
				}
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return new JSONObject(builder.toString());
	}

	/*
	 * Dichtstbijzijnde locatie ophalen
	 */
	private Location getClosestLocation() {

		ArrayList<Location> locationList = new ArrayList<Location>();
		// Resultaten uit db ophalen
		Cursor locations = db.getLocations();
		locations.moveToFirst();
		do {
			// Coordinaten splitten
			String coords = locations.getString(3);
			String[] coordinates;
			if (coords.contains(";")) {
				coordinates = coords.split(";");
			} else {
				coordinates = coords.split(", ");
			}

			// Lat en long ophalen
			double longitude = Double.parseDouble(coordinates[0]);
			double latitude = Double.parseDouble(coordinates[1]);

			// Locatie ervan bouwen
			// Zet het locatie ID in de naam van het object
			Location loc = new Location("LocatieID:"
					+ Integer.toString(locations.getInt(1)) + ":");
			loc.setLatitude(latitude);
			loc.setLongitude(longitude);

			locationList.add(loc);

		} while (locations.moveToNext());

		// Zet eerste distance erin als init
		float smallestDistance = locationList.get(0)
				.distanceTo(currentLocation);
		// Kijk hoe vaak er een nieuwe in gezet wordt
		int iterations = 0;
		for (Location l : locationList) {
			float distance = l.distanceTo(currentLocation);

			// Als er een distance kleiner is dan de eerste distance, dan wordt
			// deze erin gezet
			if (distance < smallestDistance) {
				smallestDistance = distance;
				iterations++;
			}
		}
		// Het aantal iteraties is het object met de kleinste afstand
		return locationList.get(iterations);

	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {

		return super.onStartCommand(intent, flags, startId);
	}

	private void scheduleNextUpdate(int userScheduleTime) {
		// Pending intent maken met huidige service
		Intent intent = new Intent(this, this.getClass());
		PendingIntent pendingIntent = PendingIntent.getService(this, 0, intent,
				PendingIntent.FLAG_UPDATE_CURRENT);

		// Frequentie van het checken staat op 15
		int frequency = 30;
		// Als de userinput uit settings 0 is blijft het op 15 staan
		// anders wordt het die tijd
		if (userScheduleTime != 0) {
			frequency = userScheduleTime;
		} else {
			Toast.makeText(
					getApplicationContext(),
					"Zet het peilen uit als er niet meer gecontroleed mag worden.",
					Toast.LENGTH_LONG).show();
		}

		long currentTimeMillis = System.currentTimeMillis();
		long nextUpdateTimeMillis = currentTimeMillis + frequency
				* DateUtils.MINUTE_IN_MILLIS;
		Time nextUpdateTime = new Time();
		nextUpdateTime.set(nextUpdateTimeMillis);

		// Als de volgende update voor 07:00 uur is of na 20:00
		// dan wordt de volgende tijd op 7 uur gezet
		if (nextUpdateTime.hour < 7 || nextUpdateTime.hour >= 20) {
			nextUpdateTime.hour = 7;
			nextUpdateTime.minute = 0;
			nextUpdateTime.second = 0;
			nextUpdateTimeMillis = nextUpdateTime.toMillis(false)
					+ DateUtils.DAY_IN_MILLIS;
		}

		AlarmManager alarmManager = (AlarmManager) this
				.getSystemService(ALARM_SERVICE);
		alarmManager.set(AlarmManager.RTC, nextUpdateTimeMillis, pendingIntent);
	}

}