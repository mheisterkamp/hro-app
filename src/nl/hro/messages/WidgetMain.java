package nl.hro.messages;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.widget.RemoteViews;
import android.widget.TextView;
import android.widget.Toast;

public class WidgetMain extends AppWidgetProvider {
	
	DatabaseHandler db;
	
	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager,
			int[] appWidgetIds) {
		
		super.onUpdate(context, appWidgetManager, appWidgetIds);
		
		db = new DatabaseHandler(context, null, null, 0);
		RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.widget_main);

		// Var voor laatste id
		int lastLocId;
		// Locatie op het scherm zetten
		Cursor lastLoc = db.getLastVisited();
		lastLoc.moveToFirst();
		 do {
		    	lastLocId = lastLoc.getInt(1);
		    	views.setTextViewText(R.id.widgetDepartment,
						db.getLocationName(lastLocId));
		 } while (lastLoc.moveToNext());
		 
		// Message op het scherm zetten
		Cursor lastMessage = db.getLastMessageByLocId(lastLocId);
		lastMessage.moveToFirst();
		do {
			views.setTextViewText(R.id.widgetMessage, lastMessage.getString(2));

			String msgTime = lastMessage.getString(3);
			String[] splits = msgTime.split(" ");
			String messageTime = splits[0];

			views.setTextViewText(R.id.widgetTime, messageTime);

		} while (lastMessage.moveToNext());
			
		for(int i=0;i<appWidgetIds.length;i++) {
			int appWidgetId = appWidgetIds[i];
			
            Intent intent = new Intent(context, MainActivity.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);
            views.setOnClickPendingIntent(R.id.widgetMessage, pendingIntent);
            appWidgetManager.updateAppWidget(appWidgetId, views);
		}
	}

	@Override
	public void onDeleted(Context context, int[] appWidgetIds) {
		// TODO Auto-generated method stub
		super.onDeleted(context, appWidgetIds);
		Toast.makeText(context, "HRO Widget deleted", Toast.LENGTH_SHORT).show();
	}
	
}
