package nl.hro.messages;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHandler extends SQLiteOpenHelper {

	private static final String DB_NAME = "messages";
	private static final int DB_VERSION = 1;

	public DatabaseHandler(Context context, String name, CursorFactory factory,
			int version) {
		super(context, DB_NAME, null, DB_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {

		// Deze tabel houdt de locaties bij die bezocht zijn
		db.execSQL("CREATE TABLE visited_locations ("
				+ "id INTEGER PRIMARY KEY AUTOINCREMENT, "
				+ "loc_id INTEGER NOT NULL, " 
				+ "entered INTEGER NOT NULL, "
				+ "timestamp TEXT NOT NULL )");

		// Deze tabel houdt de binnenkomende berichten bij
		db.execSQL("CREATE TABLE messages ("
				+ "id INTEGER PRIMARY KEY AUTOINCREMENT, "
				+ "loc_id INTEGER NOT NULL, " 
				+ "message TEXT NOT NULL, "
				+ "timestamp TEXT NOT NULL )");

		// De tabel met geindexeerde locaties
		db.execSQL("CREATE TABLE locations ("
				+ "id INTEGER PRIMARY KEY AUTOINCREMENT, "
				+ "loc_id INTEGER NOT NULL, "
				+ "name TEXT NOT NULL, " 
				+ "coords TEXT NOT NULL )");
	}

	@Override
	public void onOpen(SQLiteDatabase db) {
		super.onOpen(db);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS messages");
		db.execSQL("DROP TABLE IF EXISTS locations");
		db.execSQL("DROP TABLE IF EXISTS visited_locations");
		onCreate(db);
	}
	
	public Cursor getLocations() {
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.rawQuery("SELECT * FROM locations", null);
		return c;
	}
	
	public Cursor getLastVisited() {
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.rawQuery("SELECT * FROM visited_locations LIMIT 1", null);
		return c;
	}
	
	public Cursor getLastMessageByLocId(int loc_id) {
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.rawQuery("SELECT * FROM messages ORDER BY timestamp DESC LIMIT 1", null);
		return c;
	}
	
	public String getLocationName(int loc_id) {
		Cursor c = getLocations();
		c.moveToFirst();
	    do {
	    	if(c.getInt(1) == loc_id) {
	    		return c.getString(2);
	    	}
	    } while (c.moveToNext());
		
		return "";
	}

	public void insert(String table, ContentValues values) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.insert(table, null, values);
	}
	
	public void execSQL(String sql) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL(sql);
	}

}
