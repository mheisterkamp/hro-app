package nl.hro.messages;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Iterator;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.AsyncTask;

class UpdateAppDataTask extends AsyncTask<String, Void, String> {

	final static String urlLocations = "http://www.martinheisterkamp.nl/herkansing/locations.php";
	final static String urlMessages = "http://www.martinheisterkamp.nl/herkansing/messages.php";
	private DatabaseHandler db;

	public UpdateAppDataTask(DatabaseHandler handler) {
		db = handler;
	}

	@Override
	protected String doInBackground(String... str) {

		// Check of aantal locations uit de api gelijk zijn
		try {
			JSONObject data = getData(urlLocations);

			int apiLocationAmount = data.length();
			int localLocationAmount = checkAmountOfLocalLocations();

			if (apiLocationAmount > localLocationAmount) {
				// Haal de locations opnieuw op en zet ze in database
				updateLocations(data);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);
	}

	private int checkAmountOfLocalLocations() {
		Cursor locations = db.getLocations();
		return locations.getCount();
	}

	/*
	 * Locatie data ophalen
	 */
	private JSONObject getData(String url) throws JSONException {

		StringBuilder builder = new StringBuilder();
		HttpClient client = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet(url);

		try {
			HttpResponse response = client.execute(httpGet);
			StatusLine statusLine = response.getStatusLine();
			int statusCode = statusLine.getStatusCode();
			if (statusCode == 200) {
				HttpEntity entity = response.getEntity();
				InputStream content = entity.getContent();
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(content));
				String line;
				while ((line = reader.readLine()) != null) {
					builder.append(line);
				}
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return new JSONObject(builder.toString());
	}

	private void updateLocations(Object obj) {
		JSONObject object = (JSONObject) obj;
		try {
			Iterator<?> keys = object.keys();
			while (keys.hasNext()) {
				String key = (String) keys.next();
				if (object.get(key) instanceof JSONObject) {
					writeLocations((JSONObject) object.get(key));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Locaties wegschrijven in database
	 */
	private void writeLocations(JSONObject object) {
		try {
			ContentValues values = new ContentValues();
			values.put("loc_id", object.get("id").toString());
			values.put("name", object.get("locatie").toString());
			values.put("coords", object.get("coords").toString());
			db.insert("locations", values);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

}