package nl.hro.messages;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends Activity {

	// Database helper
	private DatabaseHandler db;

	// Managers en sensor
	private LocationManager locationManager;
	private SensorManager mSensorManager;
	private Sensor mSensor;

	// Locatie
	private Location currentLocation;
	private Location closestLocation;
	private float distance;
	private float currentBearing = 0;

	// Boolean voor kapotte GPS
	private final boolean GPS_BROKEN = true;
	// Lat en long uit Dordrecht
	private final double homeLat = 51.80907;
	private final double homeLong = 4.66825;

	// Compas afbeeldingen
	private ImageView imgBearing;
	private Bitmap compassNorth;
	private Bitmap compassNorthEast;
	private Bitmap compassNorthWest;
	private Bitmap compassEast;
	private Bitmap compassSouth;
	private Bitmap compassWest;

	/*
	 * Sensor listener Sensor bearing updaten en het compas updaten
	 */
	private final SensorEventListener mListener = new SensorEventListener() {
		public void onSensorChanged(SensorEvent event) {
			currentBearing = event.values[0];
			updateCompass();
		}

		public void onAccuracyChanged(Sensor sensor, int accuracy) {
		}
	};

	/*
	 * Location listener Als "gps werkt niet" op false staat, huidige locatie en
	 * dichtstbijzijnde locatie updaten
	 */
	private final LocationListener locationListener = new LocationListener() {
		public void onLocationChanged(Location location) {
			if (!GPS_BROKEN) {
				currentLocation = location;
			}
			closestLocation = getClosestLocation();

			updateCompass();
			updateDisplayInformation();
		}

		public void onStatusChanged(String provider, int status, Bundle extras) {
		}

		public void onProviderEnabled(String provider) {
		}

		public void onProviderDisabled(String provider) {
			// current location will go out of date soon, so stop showing it
			// now.
			currentLocation = null;
		}
	};

	/*
	 * Class variabelen intialiseren en methoden afgaan
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_main);

		db = new DatabaseHandler(this, null, null, 0);

		// Location manager ophalen
		locationManager = (LocationManager) this
				.getSystemService(Context.LOCATION_SERVICE);
		// Sensor manager ophalen
		mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		// Sensor orientation ophalen (depricated 4.2.2, niet 2.3.3)
		mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);

		// Eerste huidige locatie neerzetten
		currentLocation = getCurrentLocation(GPS_BROKEN);
		// Haalt de dichstbijzijnde HRO locatie op
		closestLocation = getClosestLocation();

		// De afstand ophalen
		distance = currentLocation.distanceTo(closestLocation);

		// App data updaten voordat de informatie in de app geupdate wordt
		new UpdateAppDataTask(db).execute();

		// Text van locaties en bericht updaten
		updateDisplayInformation();

		// Compas afbeeldingen in het class geheugen zetten
		getCompassGraphics();

		// Compas updaten
		// Bij gebrek aan een werkende SurfaceView die ik niet in main
		// ge�mplementeerd krijg,
		// zie CompassSurface. Deze is gelinkt via de activity_main, maar wordt
		// niet gerund of geupdate
		// anders zou ik een oranje surface moeten krijgen.
		updateCompass();

		// Service starten
		Intent i = new Intent(this, MessageService.class);
		this.startService(i);
	}

	/*
	 * Registreer location en sensor listeners
	 */
	@Override
	protected void onResume() {
		super.onResume();
		mSensorManager.registerListener(mListener, mSensor,
				SensorManager.SENSOR_DELAY_GAME);
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0,
				0, locationListener);
		currentLocation = locationManager
				.getLastKnownLocation(LocationManager.GPS_PROVIDER);
	}

	/*
	 * Haal listeners weg
	 */
	@Override
	protected void onStop() {
		locationManager.removeUpdates(locationListener);
		mSensorManager.unregisterListener(mListener);

		super.onStop();

	}

	/*
	 * Ge�mplementeerde methoden voor menu
	 */

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.stats:
			// @ todo
			return true;
		case R.id.action_settings:
			Intent i = new Intent(this, Preferences.class);
			startActivity(i);

			return true;

		default:
			return super.onOptionsItemSelected(item);
		}
	}

	/*
	 * Huidge locatie ophalen
	 */
	private Location getCurrentLocation(Boolean bool) {
		Location location;

		// Sorry Bas.. GPS emulator hield ermee op...
		if (bool == true) {
			location = new Location("Home");
			location.setLatitude(homeLat);
			location.setLongitude(homeLong);
		} else {
			LocationManager lm = (LocationManager) this
					.getSystemService(LOCATION_SERVICE);
			Criteria criteria = new Criteria();
			criteria.setAccuracy(Criteria.ACCURACY_FINE);
			String provider = lm.getBestProvider(criteria, true);
			location = lm.getLastKnownLocation(provider);
		}

		return location;
	}

	/*
	 * Compas afbeeldingen in het class geheugen zetten
	 */
	private void getCompassGraphics() {
		imgBearing = (ImageView) findViewById(R.id.bearing);
		compassNorth = BitmapFactory.decodeResource(getResources(),
				R.drawable.compass_north);
		compassNorthEast = BitmapFactory.decodeResource(getResources(),
				R.drawable.compass_north_east);
		compassNorthWest = BitmapFactory.decodeResource(getResources(),
				R.drawable.compass_north_west);
		compassEast = BitmapFactory.decodeResource(getResources(),
				R.drawable.compass_east);
		compassSouth = BitmapFactory.decodeResource(getResources(),
				R.drawable.compass_south);
		compassWest = BitmapFactory.decodeResource(getResources(),
				R.drawable.compass_west);
	}

	/*
	 * Locatie ophalen uit db -> die uit api komt Dichtstbijzijnde HRO locatie
	 */
	private Location getClosestLocation() {

		//Log.d("", "");
		
		ArrayList<Location> locationList = new ArrayList<Location>();
		// Resultaten uit db ophalen
		Cursor locations = db.getLocations();
		locations.moveToFirst();
		do {
			// Coordinaten splitten
			String coords = locations.getString(3);
			String[] coordinates;
			if (coords.contains(";")) {
				coordinates = coords.split(";");
			} else {
				coordinates = coords.split(", ");
			}

			// Lat en long ophalen
			double longitude = Double.parseDouble(coordinates[0]);
			double latitude = Double.parseDouble(coordinates[1]);

			// Locatie ervan bouwen
			// Zet het locatie ID in de naam van het object
			Location loc = new Location("LocatieID:"
					+ Integer.toString(locations.getInt(1)) + ":");
			loc.setLatitude(latitude);
			loc.setLongitude(longitude);

			locationList.add(loc);

		} while (locations.moveToNext());

		// Zet eerste distance erin als init
		float smallestDistance = locationList.get(0)
				.distanceTo(currentLocation);
		// Kijk hoe vaak er een nieuwe in gezet wordt
		int iterations = 0;
		for (Location l : locationList) {
			float distance = l.distanceTo(currentLocation);

			// Als er een distance kleiner is dan de eerste distance, dan wordt
			// deze erin gezet
			if (distance < smallestDistance) {
				smallestDistance = distance;
				iterations++;
			}
		}
		// Het aantal iteraties is het object met de kleinste afstand
		return locationList.get(iterations);

	}

	/*
	 * De textfields van de app updaten vanuit de meest recent gemeten locatie
	 */
	private void updateDisplayInformation() {
		// Naam van de locatie ophalen
		String[] splitLocation = closestLocation.toString().split(":");
		// Van de gesplitte locatienaam komt het ID;
		int targetLocationID = Integer.parseInt(splitLocation[1]);
		String targetLocationName = db.getLocationName(targetLocationID);
		// Locatie in de app zetten
		TextView closestLocationText = (TextView) findViewById(R.id.closestLocation);
		closestLocationText.setText(targetLocationName);
		// Afstand in de app zetten
		TextView textDistance = (TextView) findViewById(R.id.distance);
		textDistance.setText("~ " + Float.toString(distance) + "m");

		// Var voor laatste id
		int lastLocId;
		// Locatie op het scherm zetten
		Cursor lastLoc = db.getLastVisited();
		lastLoc.moveToFirst();
		do {
			lastLocId = lastLoc.getInt(1);
			TextView messageLocation = (TextView) findViewById(R.id.messageLocation);
			messageLocation.setText(db.getLocationName(lastLocId));
		} while (lastLoc.moveToNext());

		// Message op het scherm zetten
		Cursor lastMessage = db.getLastMessageByLocId(lastLocId);
		lastMessage.moveToFirst();
		do {
			TextView messageBody = (TextView) findViewById(R.id.messageBody);
			TextView messageDate = (TextView) findViewById(R.id.messageDate);
			TextView messageTime = (TextView) findViewById(R.id.messageTime);

			messageBody.setText(lastMessage.getString(2));
			String msgTime = lastMessage.getString(3);
			String[] splits = msgTime.split(" ");
			messageDate.setText(splits[0]);
			messageTime.setText(splits[1]);
		} while (lastMessage.moveToNext());
	}

	/*
	 * De richting van het compas updaten vanuit de meest recent gemeten locatie
	 */
	private void updateCompass() {

		// Huidige locatie ophalen
		currentLocation = getCurrentLocation(GPS_BROKEN);

		float bearing = currentLocation.bearingTo(closestLocation);

		float bearingRelative = bearing - currentBearing;
		while (bearingRelative < 0)
			bearingRelative = bearingRelative + 360;
		while (bearingRelative > 360)
			bearingRelative = bearingRelative - 360;
		if (bearingRelative <= 22) {
			imgBearing.setImageBitmap(compassNorth);
			imgBearing.setVisibility(View.VISIBLE);
		} else if (22 < bearingRelative && bearingRelative <= 68) {
			imgBearing.setImageBitmap(compassNorthEast);
			imgBearing.setVisibility(View.VISIBLE);
		} else if (68 < bearingRelative && bearingRelative <= 135) {
			imgBearing.setImageBitmap(compassEast);
			imgBearing.setVisibility(View.VISIBLE);
		} else if (138 < bearingRelative && bearingRelative <= 225) {
			imgBearing.setImageBitmap(compassSouth);
			imgBearing.setVisibility(View.VISIBLE);
		} else if (225 < bearingRelative && bearingRelative <= 295) {
			imgBearing.setImageBitmap(compassWest);
			imgBearing.setVisibility(View.VISIBLE);
		} else if (295 < bearingRelative && bearingRelative <= 338) {
			imgBearing.setImageBitmap(compassNorthWest);
			imgBearing.setVisibility(View.VISIBLE);
		} else if (338 < bearingRelative) {
			imgBearing.setImageBitmap(compassNorth);
			imgBearing.setVisibility(View.VISIBLE);
		}
	}

}
