package nl.hro.messages;

import android.R.color;
import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class CompassSurface extends SurfaceView implements
		SurfaceHolder.Callback {

	private SurfaceThread thread;

	public CompassSurface(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.getHolder().addCallback(this);
		// Thread aanmaken om acties op andere thread te laten draaien
		thread = new SurfaceThread(this.getHolder());
	}
	
	// Thread bereikbaar maken
	public SurfaceThread getThread() {
		return thread;
	}


	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		// Thread locken voor de draw functie
		thread.setRunning(true);
		thread.start();
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		boolean retry = true;
		thread.setRunning(false);
		while (retry) {
			try {
				thread.join();
				retry = false;
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	}
	
	class SurfaceThread extends Thread {

		private boolean running;
		private SurfaceHolder surfaceHolder;

		public SurfaceThread(SurfaceHolder s) {
				surfaceHolder = s;
		}
		
		public void setRunning(boolean condition) {
			running = condition;
		}

		@Override
		public void run() {
			Canvas c;

			while (running) {
				c = null;
				try {
					c = surfaceHolder.lockCanvas(null);
					synchronized (surfaceHolder) {
						drawCanvas(c);
					}
				} finally {
					if(c != null) {
						surfaceHolder.unlockCanvasAndPost(c);
					}
				}
			}
		}

		public void drawCanvas(Canvas canvas) {
			canvas.drawColor(color.holo_orange_light);
		}
	}

}
